const expect = require("expect");

const {generateMessage, generateLocationMessage} = require("./message");

describe("generateMessage", () => {
  it('should generate the correct message object',  () => {
    var from = 'jen';
    var text = 'Some Message';

    var message = generateMessage(from, text);

    expect(message.createdAt).toBeA('number');
    expect(message).toInclude({
      from,
      text
    });
  });
});

describe("generateLocationMessage", () => {
  it('should generate the correct location object',  () => {
    var from = 'jen';
    var latitude = 15;
    var longitude = 20;
    var url = 'https://www.google.com/maps?q=15,20';
    var message = generateLocationMessage(from, latitude, longitude);

    expect(message.createdAt).toBeA('number');
    expect(message).toInclude({
      from,
      url
    });
  });
});
