const expect = require("expect");

const {Users} = require("./users");

describe("Users", () => {
  var users;
  beforeEach(() => {
    users = new Users();
    users.users = [{
      id: '1',
      name: 'mike',
      room:'Node course'
    }, {
      id: '2',
      name: 'jen',
      room:'React course'
    }, {
      id: '3',
      name: 'julie',
      room:'Node course'
    }];
  });

  it('should add new user', function () {
    var users = new Users();
    var user = {
      id: '123',
      name: 'Naveen',
      room: 'The office fans'
    };
    var resUser = users.adduser(user.id, user.name, user.room);

    expect(users.users).toEqual([user]);
  });

  it('should remove a user', function () {
    var userId = '1';
    var user = users.removeUser(userId);

    expect(user.id).toBe(userId);
    expect(users.users.length).toBe(2);
  });

  it('should not remove a user', function () {
    var userId = "99";
    var user = users.removeUser(userId);

    expect(user).toNotExist(userId);
    expect(users.users.length).toBe(3);
  });

  it('should find user', function () {
    var userId = '2';
    var user = users.getUser(userId);

    expect(user.id).toBe(userId);
  });

  it('should not find user', function () {
    var userId = '2345';

    var user = users.getUser(userId);

    expect(user).toNotExist();
  });

  it('should return names for node course', function() {
    var usersList = users.getUsersList('Node course');

    expect(usersList).toEqual(['mike', 'julie']);
  });

  it('should return names for react course', function() {
    var usersList = users.getUsersList('React course');

    expect(usersList).toEqual(['jen']);
  });

});
