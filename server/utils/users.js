//adduser(id, name, room)
//removeUser(id)
//fetchUser(id)
//getUserList(roomName)

class Users {
  constructor(){
    this.users = [];
  }

  adduser (id, name, room){
    var user = {id,name,room};
    this.users.push(user);
    return user;
  }

  removeUser (id) {
    //return user that was removed
    var user = this.getUser(id);
    console.log(user);
    if(user) {
      this.users = this.users.filter((user) => user.id !== id);
    }

    return user;
  }

  getUser (id) {
    //return user obj
    return this.users.filter((user) => user.id === id)[0];
  }

  getUsersList (room){
    var users = this.users.filter((user) => user.room === room);
    var namesArr = users.map((user)=> user.name);

    return namesArr;
  }
}

module.exports = {Users};

// class Person {
//   constructor(name, age){
//     this.name = name;
//     this.age = age;
//   }
//
//   getUserDescription(){
//     return `${this.name} is ${this.age} year(s) old.`
//   }
// }
//
// var me = new Person('Naveen', 26);
// console.log(me.name, me.age);
// console.log(me.getUserDescription());
